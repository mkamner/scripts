# My Scripts

This is a currated collection of useful little snippets & functions for various languages I wrote over the years.

Snippets including any code from 3rd parties have attribution inline to those sections.

Happy coding!

I work on this in my spare time, if it helped you solve a problem or enhance your skills
I would appreciate it if you would consider [buying me a coffee](https://www.buymeacoffee.com/ProfessorLogout).

## ps1

PowerShell scripts & ressources

## ipsum.md

Lorem Ipsum files for testing various command line tools and usage in reproducable examples in my [blog](https://ps1.guru).

Generated from jaspervdj's [lorem-markdownum](https://github.com/jaspervdj/lorem-markdownum#http-api) using this PowerShell snippet:

```powershell
$uri = "https://jaspervdj.be/lorem-markdownum/markdown.txt?fenced-code-blocks=on&no-external-links=on"
0..9 | ForEach-Object {(Invoke-WebRequest -Uri $uri).Content >> "$_.md"}
```

Some stats about those sample files:

```powershell
PS ./ipsum.md> Get-ChildItem | Foreach-Object {Get-Content $_ | Measure-Object -Line -Word -Character}

Lines Words Characters Property
----- ----- ---------- --------
   47   311       2383
   43   291       2261
   29   249       1671
   44   287       2115
   82   446       3596
   58   323       2655
   29   268       1798
   38   357       2443
   53   307       2577
   35   293       2034
```
