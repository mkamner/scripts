function mr {
    <#
    .Synopsis
    Create Merge Requests by pushing to GitLab
    .Description
    Create Merge Requests in GitLab by sending git -o options while pushing
    .Example
    C:\PS> mr "New things"
    .Example
    C:\PS> mr "New things" -Draft
    .Example
    C:\PS> mr "New things" -AutoMerge
    .Notes
    Author: Marco Kamner; marco@kamner.de
    .Link
    https://gitlab.com/mkamner/scripts
    .Outputs
    Output of git push
    #>
    [CmdletBinding()]
    param (
        [Parameter(Position=0)]
        [String]
        $Name,
        [Parameter()]
        [Switch]
        $Draft = $false,
        [Parameter()]
        [Switch]
        $AutoMerge = $false
    )
    process {
        if ($Draft) {
            # Push the current branch to be a draft mr
            git push --set-upstream origin HEAD -o merge_request.create -o merge_request.title="Draft: $Name"
        } elseif ($AutoMerge -and -not $Draft) {
            # Auto merge mr if CI/CD pipeline succeeds, but only if it's not supposed to be a draft
            git push --set-upstream origin HEAD -o merge_request.create -o merge_request.title="$Name" -o merge_request.merge_when_pipeline_succeeds
        } else {
            # Just create a mr
            git push --set-upstream origin HEAD -o merge_request.create -o merge_request.title="$Name"
        }
    }
}
