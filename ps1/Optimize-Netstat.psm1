function Optimize-Netstat {
    <#
    .Synopsis
    Optimize Netstat output
    .Description
    Adds process namens to netstat output
    .Example
    C:\PS> netstat -o | Optimize-Netstat

    Add process name behind the id
    .Example
    C:\PS> netstat -o | Optimize-Netstat -removeProcessId
       
    Replace process id by name
    .Example
    C:\PS> Optimize-Netstat

    Runs netstat and optimizes the output to contain process name and id
    .Notes
    Author: Marco Kamner; marco@kamner.de
    .Link
    https://gitlab.com/mkamner/scripts
    .Inputs
    Output of netstat
    .Outputs
    Modified output of netstat
    #>
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $false, ValueFromPipeline = $true)]
        $netstat,
        [Parameter(Mandatory = $false)]
        [Switch]
        $removeProcessId
    )
    
    begin {
        $processList = Get-Process

        if (-not $netstat) {
            $netstat = netstat -o
        }
    }
    process {
        foreach ($result in $netstat) {
            $splitArray = $result -split " "
            $processId = $splitArray[$splitArray.length - 1]
            $processName = $processList | Where-Object { $_.id -eq $processID } | Select-Object -Property "processname"
            if ($removeProcessId -eq $false) {
                $transformedName = "$processID $($processName.processname)"
            }
            else {
                $transformedName = $processName.processname
            }
            $splitArray[$splitArray.length - 1] = $transformedName
            $line = $splitArray -join " "
            Write-Output $line
        }
    }
}
