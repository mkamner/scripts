function Get-CommandUsage {
    <#
    .Synopsis
    Count commandlet occurences in your history
    .Description
    Extract all commandlets with standard verbs from your history and count them up.
    Works with commandlets in any part of your history entries.
    .Example
    C:\PS> Get-CommandUsage
    .Notes
    Author: Marco Kamner; marco@kamner.de
    .Link
    https://gitlab.com/mkamner/scripts
    .Outputs
    Name and occurance count of commandlet in history
    #>
    process {
        # Get history and verbs
        $history = Get-History
        $verbs = Get-Verb

        # Build regex pattern containing all verbs from the system
        $regexVerbs = $verbs.verb.ToLower() -join "|"
        $pattern = "($regexVerbs)-\w+"

        # Extract all matches from the history
        $historyMatches = $history | Select-Object -Property "CommandLine" | Select-String -Pattern $pattern -AllMatches

        # Get all matches grouped up
        $commands = $historyMatches | ForEach-Object { Write-Output $_.Matches.Value }

        # Group and output
        $weightedCommands = $commands | Group-Object | Sort-Object -Property "Count" -Descending | Select-Object -Property "Count", "Name"
        return $weightedCommands
    }
}
