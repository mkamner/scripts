function wc {
    <#
    .Synopsis
    WC for PowerShell
    .Description
    Can count lines, words and characters in files
    .Example
    PS> wc -Path test.md -Line -Word -Character

    Calculate line, word and character count for test.md
    .Example
    PS> wc *.md -l
       
    Calculate total line count for all files matching *.md in current directory
    .Notes
    Author: Marco Kamner; marco@kamner.de
    .Link
    https://gitlab.com/mkamner/scripts
    .Outputs
    Same as Measure-Object
    #>
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, Position = 0)]
        [String]
        $Path,
        [Parameter()]
        [Alias("l")]
        [switch]
        $Line,
        [Parameter()]
        [Alias("w")]
        [switch]
        $Word,
        [Parameter()]
        [Alias("c")]
        [switch]
        $Character

    )
    process {
        $PSBoundParameters.Remove("Path") | Out-Null
        Get-Content -Path $Path | Measure-Object @PSBoundParameters

    }
}
