#! /bin/bash
# Returns the current timestamp and HTTP response code obtained by curl on the URL using the specified HTTP method, repeating every DELAY seconds
# Usage: script.sh GET https://example.org 5
# Usage: scripts.sh POST https://example.org 1 | tee -a persistent-file.log
# Marco Kamner <marco@kamner.de>
# Link: https://gitlab.com/mkamner/scripts

METHOD=$1
URL=$2
DELAY=$2

while true; do
    TIMESTAMP=$(date +"%Y-%m-%d %H:%M:%S")
    echo "$TIMESTAMP $(curl --silent --output /dev/null --write-out '%{http_code}' --connect-timeout 5 --request $METHOD $URL)"
    sleep $DELAY
done
