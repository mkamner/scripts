# Gitlab utilities for bash
# Description: Drop-in utils ment to be added to the users profile
# Author: Marco Kamner; marco@kamner.de
# Link: https://gitlab.com/mkamner/scripts

dmr() {
    # Push the current branch to be a draft mr
    git push --set-upstream origin HEAD -o merge_request.create -o merge_request.title="Draft: $1"
}

mr() {
    # Just create a mr
    git push --set-upstream origin HEAD -o merge_request.create -o merge_request.title="$Name"
}

mrmp() {
    # Auto merge mr if CI/CD pipeline succeeds
    git push --set-upstream origin HEAD -o merge_request.create -o merge_request.title="$1" -o merge_request.merge_when_pipeline_succeeds
}
