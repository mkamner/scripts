#!/bin/bash
# Merge multiple kube config files into one for use with kubectl
# Author: Marco Kamner <marco@kamner.de>
# Link: https://gitlab.com/mkamner/scripts

set -e

CONFIGS_DIR=~/.kube/configs
MERGE_FILE=~/.kube/configs/_merged
KUBE_CONFIG=~/.kube/config

# Merge all configs in this folder into a config file for use by kubectl
yq eval-all '. as $item ireduce ({}; . *+ $item )' $CONFIGS_DIR/*.yaml > $MERGE_FILE

# Backup current config
printf -v datestamp '%(%Y-%m-%d-%H-%M-%S)T'
cp $KUBE_CONFIG "$KUBE_CONFIG.$datestamp"

# Overwrite the old config
cp $MERGE_FILE $KUBE_CONFIG
